<?php 

    /* Template Name: Contact */ 

    get_header(); 
    
?>

<section role="main" id="contact-page" class="main">
        
    <div class="banner" data-speed="5" data-type="background">
        <div>
            <div class="title-border-bottom"></div>

            <div class="address">
                FH Studio<br />
                1410 William<br />
                H3C 1R5  Montreal<br />
                Qu&eacute;bec, Canada
            </div>

            <a href="" class="button map">View Map</a>

            <div class="tel">
                T. +1 514 989 - PROD (7763) / 514 574-8901<br />
                E. <span class="email">info@fh-studio.com</span>
            </div>
        </div>

    </div>

    <article>
        
        <div class="title-border-bottom"></div>

        <div class="write-us">Write us something</div>
        <div class="start-typing">Start typing</div>
        
        <textarea name="message" id="your-message"></textarea>
        
        <input type="text" name="name" placeholder="Name*" />
        
        <input type="text" name="email" placeholder="Email*" />
        
        <button type="button">Send</button>

    </article>
    
</section>

<?php get_footer('social'); ?>

<script type="text/javascript">
    
    $(document).ready(function(){
        
        if($('.banner').length){
            $('.banner').css('opacity','0');
            $('.address').css('opacity','0');
            $('.tel').css('opacity','0');
            $('.title-border-bottom').css('position','relative');
            $('.title-border-bottom').css('left','-300px');
            setTimeout(function(){
                $('.banner').animate({opacity: 1},300);
            },300);
            setTimeout(function(){
                $('.address').animate({opacity: 1},300);
            },900);
            setTimeout(function(){
                $('.tel').animate({opacity: 1},300);
            },1200);
            setTimeout(function(){
                $('.title-border-bottom').animate({left: '0'},300);
            },1600);
        }
        
        var $window = $(window);
        
        $('.banner[data-type="background"]').each(function(){
            var $bgobj = $(this);

            $(window).scroll(function() {
                    
                var top = $bgobj.offset().top;
                
                if($window.scrollTop()+100 >= top){
                
                    $('.banner').addClass('notransition');

                    var yPos = -($window.scrollTop() / $bgobj.data('speed')) + top;
                    var coords = '50% '+ yPos + 'px';

                    $bgobj.css({ backgroundPosition: coords });
                    $bgobj.css({ backgroundAttachment: 'fixed'});
                
                }else{
                    $bgobj.css({ backgroundPosition: '50% 0' });
                    $bgobj.css({ backgroundAttachment: 'scroll'});
                }

            });

        });
        
        var $contactTop = $('article').offset().top;
        var start = false;
        
        $window.scroll(function() {
            if(($contactTop-$window.height()+500 < $window.scrollTop()) && start === false){
                start = true;
                $('#your-message').focus();
                setTimeout(function(){$('#your-message').val("H")},100);
                setTimeout(function(){$('#your-message').val("He")},300);
                setTimeout(function(){$('#your-message').val("Hel")},400);
                setTimeout(function(){$('#your-message').val("Hell")},700);
                setTimeout(function(){$('#your-message').val("Hello")},1000);
                setTimeout(function(){$('#your-message').val("Hello F")},1100);
                setTimeout(function(){$('#your-message').val("Hello FH")},1200);
                setTimeout(function(){$('#your-message').val("Hello FH-")},1500);
                setTimeout(function(){$('#your-message').val("Hello FH-S")},1700);
                setTimeout(function(){$('#your-message').val("Hello FH-St")},1800);
                setTimeout(function(){$('#your-message').val("Hello FH-Stu")},2000);
                setTimeout(function(){$('#your-message').val("Hello FH-Stud")},2100);
                setTimeout(function(){$('#your-message').val("Hello FH-Studi")},2200);
                setTimeout(function(){$('#your-message').val("Hello FH-Studio")},2500);
                setTimeout(function(){$('#your-message').val("Hello FH-Studio,")},2600);
                setTimeout(function(){$('#your-message').val("Hello FH-Studio,\n")},2600);
            }
        });
        
    });
    
</script>