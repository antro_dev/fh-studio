<?php get_header(); ?>
	
	<!-- section -->
	<section role="main" class="posts main">
	
		<?php get_template_part('loop'); ?>
		
		<?php get_template_part('pagination'); ?>
	
	</section>
	<!-- /section -->

<?php get_footer('empty'); ?>