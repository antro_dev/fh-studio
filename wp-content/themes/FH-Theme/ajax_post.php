<div class="title-border-bottom"></div>

<h2>Articles</h2>


<?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post(); ?>

    <div class="project">
        <a href="<?php echo get_permalink() ?>" style=" background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>');">

        </a>
        <div>
            <h3><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></h3>
            <span><?php echo the_date(); ?></span>
        </div>
    </div>

    <?php endwhile; ?>

<?php else: ?>

<p>There is no article related to your research.</p>

<?php endif; ?>