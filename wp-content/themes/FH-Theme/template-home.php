<?php 

    /* Template Name: Home page */ 

    get_header('light'); 
    
?>

        <section id="home" data-speed="5" data-type="background">
             
            <?php
                $args = array(
                    'post_type' => 'projects',
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'meta_query' => array(
                        array(
                            'key' => 'wpcf-featured-project',
                            'value' => 1,
                            'compare' => 'is',
                        ),
                    ),
                    'posts_per_page' => 5
                );
            ?>
            <?php $slides = new WP_query($args); ?>
            <?php if ($slides) : ?>
                <style>
                <?php $i = 1 ?>
                <?php while ($slides->have_posts()) : $slides->the_post(); ?>
                    .cb-slideshow li:nth-child(<?php echo $i ?>) > span {
                        background-image:url('<?php echo get_post_meta($post->ID,'wpcf-image',TRUE) ?>');
                    }
                    <?php $i++; ?>
                <?php endwhile; ?>
                </style>

                <ul class="cb-slideshow">
                <?php $j = 1; while ($slides->have_posts()) : $slides->the_post(); ?>
                    
                    <li>
                        <span>
                            <div<?php echo (get_post_meta($post->ID,'wpcf-main-video',TRUE))? ' class="video"' : '' ?>>
                                <?php if($video = get_post_meta($post->ID,'wpcf-main-video',TRUE)): ?>
                                    <iframe src="http://player.vimeo.com/video/<?php echo $video ?>?color=f01776&api=1&player_id=player-<?php echo $j ?>" width="100%" height="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                                <?php endif; ?>
                            </div>
                        </span>
                    </li>
                    
                <?php $j++; endwhile; ?>
                </ul>
            <?php endif; ?>
            
<!--                <div class="logo"><h1>FH Studio</h1></div>-->
                <div class="home-bg">
                    <div class="title-border-top"></div>
                    <div class="slogan">On prend<br />vos clics <br />et vos claps</div>
                    <div class="title-border-bottom"></div>

                    <div class="scroll-down">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div>SCROLL</div>
                    </div>
                </div>
            
        </section>
            
        <section id="content" class="home">
       
            <header>
                
                <?php
                    if($_COOKIE['bookmark']){
                        global $wpdb;
                        $bookCount = $wpdb->get_var('SELECT COUNT(*) FROM wp_bookmarks WHERE cookie_id = ' . $_COOKIE['bookmark'] . ' OR id = ' . $_COOKIE['bookmark']);
                    }
                ?>
                
                <div class="top-nav">
                    <ul>
                        <li><a href="#" class="newsletter">Newsletter</a></li>
                        <li<?php echo $bookCount? ' class="bandeau"' : ''; ?>>
                            <a href="<?php echo get_permalink(1875) ?>">
                                <?php if($bookCount): ?>
                                <div><?php echo $bookCount ?></div>
                                <?php endif; ?>
                                Bookmarks
                            </a>
                        </li>
                        <li class="search"><a href="#">Search</a></li>
                        <li><a href="#">Fran&ccedil;ais</a></li> 
                        <li class="social facebook"><a href="http://facebook.com/fhstudio" target="_blank"></a></li>
                        <li class="social twitter"><a href="http://twitter.com/fhstudi0" target="_blank"></a></li>
                        <li class="social pinterest"><a href="http://www.pinterest.com/fhstudi0" target="_blank"></a></li>
                    </ul>
                </div>
                
                <a href="<?php echo get_bloginfo('url') ?>" class="logo-link"><div class="logo"></div></a>
                
                <nav class="nav" role="navigation">
                        <?php wp_nav_menu( array( 'menu' => 'main' ) ); ?>
                </nav>
            </header>
                        
            <div id="search-box">
                            
                    <form name="search-form" method="post" action="#">

                        <div class="close-search"></div>

                        <div class="title-border-bottom"></div>

                        <div class="search-for">Search On FH-studio</div>
                        <div class="start-typing">Start typing</div>

                        <input type="text" name="search" id="search-input" value="" placeholder="Search..." autocomplete="off" />

                        <div class="type">
                            <div>
                                <input type="radio" name="type" value="projects" id="projects-type" checked="checked" /><label for="projects-type" class="selected underline">Projects</label>
                            </div>
                            <div>
                                <input type="radio" name="type" value="talents" id="artists-type" /><label for="artists-type" class="underline">Artists</label>
                            </div>
                            <div>
                                <input type="radio" name="type" value="post" id="posts-type" /><label for="posts-type" class="underline">Articles</label>
                            </div>
                        </div>

                    </form>

                    <div id="search-result"></div>
                </div>
            
            <?php
                $args = array(
                    'post_type' => 'projects',
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                );
            ?>
            <?php $projects = new WP_query($args); ?>
            <?php if ($projects) : ?>
            
            <div class="project-container main">
                
                <ul>
                    
                <?php while ($projects->have_posts()) : $projects->the_post(); ?>
                
                <?php $parent_talent_id = wpcf_pr_post_get_belongs($post->ID, 'talents') ?>
                
                <li class="project">
                    <a class="normal" href="<?php echo get_permalink() ?>" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>');">
                        <div<?php echo (get_post_meta($post->ID,'wpcf-type',TRUE) == 'video' || get_post_meta($post->ID,'wpcf-type',TRUE) == 'both')? ' class="video"' : '' ?>>
                            <div>
                                <h3><?php the_title(); ?></h3>
                                <span><?php echo get_the_title(($parent_talent_id)? $parent_talent_id : 'Production') ?></span>
                            </div>
                        </div>
                    </a>
                    <div class="background"></div>
                </li>
                
                <?php endwhile; ?>
                
                </ul>
                
            </div>
            
            <?php endif; ?>
            
        </section>

<?php get_footer('empty'); ?>

<script type="text/javascript">
    
    var divProjectsTop = 0;
    
    $(document).ready(function(){
    
        var windowW = $(window).width();
        var windowH = $(window).height();
        
        resizeProjectBoxes();
        
        divProjectsTop = $('.project-container').offset().top;
        addRollover();
        $('.slogan').css('margin-top', 270*windowH/450-250);
        $('#home .title-border-top').css('margin-top', 270*windowH/450-250-30);
        $('#home .title-border-bottom').css('margin-top', 270*windowH/450-250+185);
        $('#home .scroll-down').css('margin-top', 270*windowH/450-250+175+65);
        $('.slogan').css('margin-left', 305*windowW/1600);
        
        var marginL = $('.slogan').offset().left;
        $('#home .title-border-top').css('left', marginL);
        $('#home .title-border-bottom').css('left', marginL);
        $('#home .scroll-down').css('left', marginL-10);
        
        var positionX = $('.slogan').offset().left;
        var positionY = $('.slogan').offset().top;

    });
    
    var count = 2;
    
    $(window).scroll(function(){  
        if  ($(window).scrollTop() == $(document).height() - $(window).height()){  
           loadArticle(count);
           count++;  
        }  
    });

    $(window).resize(function(){
        resizeProjectBoxes();
    });
    
    function loadArticle(pageNumber) {  
        $.ajax({  
            url: "<?php bloginfo('wpurl') ?>/wp-admin/admin-ajax.php",
            type:'POST',  
            data: "action=infinite_scroll&page_no="+ pageNumber + '&loop_file=project&type=all',   
            success: function(html){  
                $(".project-container ul").append(html);
                loaded = false;
                resizeProjectBoxes();
                addRollover();
            }  
        });  
        return false;  
    }
</script>
	