<div class="title-border-bottom"></div>

<h2>Projects</h2>

<?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post(); ?>

    <?php $parent_talent_id = wpcf_pr_post_get_belongs($post->ID, 'talents') ?>

    <div class="project">
        <a href="<?php echo get_permalink() ?>" style=" background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>');">

        </a>
        <div>
            <h3><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></h3>
            <span><?php echo get_the_title(($parent_talent_id)? $parent_talent_id : 'Production') ?></span>
        </div>
    </div>

    <?php endwhile; ?>

<?php else: ?>

<p>There is no project related to your research.</p>

<?php endif; ?>