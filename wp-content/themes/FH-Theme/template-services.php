<?php 

    /* Template Name: Services */ 

    get_header(); 
    
?>

<section role="main" id="services-page" class="main">
	
    <div class="production">
        
        <div class="banner" data-speed="5" data-type="background">
            <div>
                <div class="title-border-top"></div>

                <h1>
                    <?php echo __('Prod<br/>ucti</br>on') ?>
                </h1>

                <div class="title-border-bottom"></div>
            </div>
        </div>

        <?php 
            $post = get_page(129); 
            $content = apply_filters('the_content', $post->post_content);
        ?>

        <article>
            
                <?php //echo $content; ?>
                
                <div class="left">
                    <div class="title-border-bottom"></div>
                    <p>When it comes to photography and video production, we’ve got it down to an art form. For you this means a seamless production process, where every detail no matter how small, is covered — from turnkey projects to intensive collaboration with your team.</p> 

                    <p>Whether you’re embarking on a small or large scale production, our team of award- winning directors, cinematographers and photographers are 100% dedicated to meeting your exacting demands. All this and our A-list of contacts along with our sister company H films – a renowned Moroccan film production company – guarantees that you’ll always be in good hands in Canada & Morocco.</p>
                    
                    <div class="line"></div>
                    
                    <div class="requirements">
                        <p>For your photography requirements , our team is composed of:</p>
                        <ul>
                            <li>Fashion Photographer</li>
                            <li>Advertising Photographer</li>
                            <li>Product Photographer</li>
                            <li>Food and Culinary Photographer</li>
                        </ul>

                    </div>
                </div>
                
                <div class="right">
                    <p class="title">Our Photography and Video Production Services</p>
                    
                    <div>
                        <ul>
                            <li>Project coordination</li>
                            <li>Budget evaluation</li>
                            <li>Production plans & Time-lines</li>
                            <li>Management of turnkey projects</li>
                        </ul>
                        <ul>
                            <li>Locations</li>
                            <li>Interior & Exterior</li>
                            <li>Local & international</li>
                            <li>Permits & Security</li>
                        </ul>
                    </div>
                    <div>
                        <ul>
                            <li>Talent management</li>
                            <li>Directors</li>
                            <li>Photographers</li>
                            <li>Cinematographers</li>
                        </ul>
                        <ul>
                            <li>Studio</li>
                            <li>Photo Studio Rental</li>
                            <li>Equipment rental Take a Look at our Video & Photography</li>
                            <li>Lighting and gripping equipments</li>
                        </ul>
                    </div>
                    <div>
                        <ul>
                            <li>Production team</li>
                            <li>Stylists, Hair & Make-up artists,</li>
                            <li>Grip, Electro, Sound Man, Producers</li>
                            <li>Props stylists and set decorators, assistants</li>
                            <li>Caterers etc..</li>
                        </ul>
                        <ul>
                            <li>Castings</li>
                            <li>Models</li>
                            <li>Actors & extras</li>
                        </ul>
                    </div>
                    <div>
                        <ul>
                            <li>Post Production</li>
                            <li>Photography manipulation</li>
                            <li>Video editing</li>
                            <li>CGI / VFX</li>
                            <li>Sound design / Recording</li>
                            <li>Sound Studio</li>
                        </ul>
                        <ul>
                            <li>Travel arrangement</li>
                            <li>Air and road transportation</li>
                            <li>Permits and insurances</li>
                            <li>Accommodations & Meals</li>
                        </ul>
                    </div>

                </div>

        </article>
                
    </div>
    
    <div class="studio-rental">
        <div class="banner larger" data-speed="5" data-type="background">
            <div>
                <div class="title-border-top"></div>

                <h1>
                    Studio</br>rental
                </h1>

                <div class="title-border-bottom"></div>
            </div>
        </div>

        <?php 
            $post = get_page(131); 
            $content = apply_filters('the_content', $post->post_content);
        ?>

        <article>

            <div class="content">
                <?php // echo $content; ?>
                
                <div class="left">
                    <div class="title-border-bottom"></div>
                    <p class='title'>Video and Photography Studio Rental</p>

                    <p>Because we’re a group of photographers, cinematographers and producers, we know space like no one else. Our studios are designed around you and your needs, so that you can maximize productivity — and all this in a comfortable environment that will make you and your clients feel completely at ease. Both video and photography studios are open daily from 9 a.m. — 6 p.m. with an option for early / late shoots.
                    We also can provide all kind of equipment for you so everything will be ready at the studio for your shoot.</p> 
                    
                    <a target="_blank" href="<?php echo (ICL_LANGUAGE_CODE == 'EN')? 'http://widtech.info/fh-studio/wp-content/uploads/2013/10/Fiche-Tech-Fh-Studio-EN-.pdf' : 'http://widtech.info/fh-studio/wp-content/uploads/2013/10/Fiche-Tech-Fh-Studio-FR-.pdf' ?>" class="button"><?php echo __('Download the technical specs') ?></a>
                </div>
                
                <div class="right citation">
                    <p>“FH Studio was the best place for me in town, right after the fairmount bagel ... they’re pretty badass aren’t they ?”</p>

                    <p class="author">_John kevin Le citationniste</p>
                </div>
                
            </div>

        </article>
        
        <?php
            $args = array(
                'post_type' => 'studios',
                'orderby' => 'title',
                'order' => 'ASC',
            );
        ?>
        <?php query_posts($args); ?>

        <div class="studios">

            <?php if (have_posts()) : ?>

                <?php while (have_posts()) : the_post(); ?>

                <div>
                    <div class="info" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>'); ">
                        
                        <div class="title-border-bottom"></div>
                        <div class="title">
                            Studio <span><?php the_title(); ?></span>
                        </div>
                        <div class="content">
                            <?php the_content(); ?>
                        </div>
                        
                    </div>
                    
                    <div class="photos">
                        <?php $i = 0; foreach(get_post_meta($post->ID,'wpcf-studio-photos') as $photo): ?>
                        <div style="background-image: url('<?php echo $photo ?>');"></div>
                        <?php $i++; endforeach; ?>
                        
                        <?php if($i%3 != 0): ?>
                        
                            <?php if($i%3 == 2): ?>
                                <div class="empty"></div>
                            <?php elseif($i%3 == 1): ?>
                                <div class="empty"></div>
                                <div class="empty"></div>
                            <?php endif; ?>
                        <?php endif; ?>
                        
                    </div>
                    
                </div>

                <?php endwhile; ?>

            <?php endif; ?>

        </div>
                
    </div>
    
</section>

<?php get_footer('empty'); ?>

<script type="text/javascript">
    
    $(document).ready(function(){
        
        resizeTitleBorders();
        
        if($('.banner').length){
            $('.banner').css('opacity','0');
            $('h1').css('opacity','0');
            $('h2').css('opacity','0');
            $('.title-border-top').css('position','relative');
            $('.title-border-top').css('left','-1000px');
            $('.title-border-bottom').css('position','relative');
            $('.title-border-bottom').css('left','-1000px');
            setTimeout(function(){
                $('.banner').animate({opacity: 1},300);
            },300);
            setTimeout(function(){
                $('h1').animate({opacity: 1},300);
                $('h2').animate({opacity: 1},300);
            },900);
            setTimeout(function(){
                $('.title-border-top').animate({left: '0'},300);
            },1100);
            setTimeout(function(){
                $('.title-border-bottom').animate({left: '0'},300);
            },1300);
        }
        
        var $window = $(window);
        
        $('.banner[data-type="background"]').each(function(){
            var $bgobj = $(this);
            var top = $bgobj.offset().top;

            $(window).scroll(function() {
                
                // data-type="background"
                
                if($window.scrollTop()+100 >= top){
                
                    $('.banner').addClass('notransition');
                    
//                    console.log($window.scrollTop());

                    var yPos = -(($window.scrollTop()) / $bgobj.data('speed'));
                    var coords = '50% '+ yPos + 'px';
                    
//                    console.log(yPos);

                    $bgobj.css({ backgroundPosition: coords });
                    $bgobj.css({ backgroundAttachment: 'fixed'});
                
                }else{
                    $bgobj.css({ backgroundPosition: '50% 0' });
                    $bgobj.css({ backgroundAttachment: 'scroll'});
                }

            });

        });
        
        var contentLeft = $('article .content').offset().left;
        $('article .title-border-bottom').css('margin-left',contentLeft);
        
        var windowWidth = $window.width();
        $('.studios .photos > div').css('width', windowWidth/2/3);
        $('.studios .photos > div').css('height', windowWidth/2/3);
        
    });
    
</script>