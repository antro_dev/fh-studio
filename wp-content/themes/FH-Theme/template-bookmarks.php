<?php 
    /* Template Name: Bookmarks */ 
    get_header(); 
?>
	
	<!-- section -->
	<section role="main" id="project-page" class="main bookmark">
            
            <div>
            <?php 
                global $wpdb;
                $bookmarks = $wpdb->get_results('SELECT * FROM wp_bookmarks WHERE cookie_id = ' . $_COOKIE['bookmark'] . ' OR id = ' . $_COOKIE['bookmark']); 
            ?>
                
                <?php if($_COOKIE['bookmark']): ?>
	
                <div class="infos">
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <div class="sticky">

                            <div class="title-border-bottom"></div>

                            <h1>
                                <?php the_title(); ?>
                            </h1>

                            <div class="description"><?php the_content() ?></div>
                           
                            <div class="puces">

                                <?php $i = 0 ?>
                                <?php foreach($bookmarks as $book): ?>

                                <a href="#book-<?php echo $i ?>"><div class="puce"></div></a>

                                <?php $i++; endforeach; ?>

                            </div>

                        </div>

                    <?php endwhile; ?>

                    <?php endif; ?>
                </div>

                <div class="content">

                    <?php $i=0; foreach($bookmarks as $bookmark): ?>
                    
                    <div class="project-photo project" id="book-<?php echo $i ?>" data-project-id="<?php echo $bookmark->project_id ?>" data-project-type="<?php echo $bookmark->type ?>" data-project-content="<?php echo $bookmark->content ?>">
                        <div class="delete"></div>
                        <img src="<?php echo $bookmark->content ?>" />
                    </div>

                    <?php $i++; endforeach; ?>

                </div>
                
                <?php endif; ?>
                
            </div>
	
	</section>
	<!-- /section -->
	
<!--<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.unveil.min.js"></script>-->
<script type="text/javascript">
    
    var idPuce;
    var stickyH = $('.sticky').height()+120;
    var divProjectsTop = 0;
    
    $(window).scroll(function(){
        var top = $(this).scrollTop();

        var newTop = top+100;
        $('.sticky').css({top: newTop});
        
        $('.project-photo').each(function(){
            var dataTop = $(this).offset().top;
            if(dataTop <= top+105){
                idPuce = $(this).attr('id');
            }
        });
        
        if(idPuce){
            $('.puce').removeClass('selected');
            $('a[href="#' + idPuce + '"] > div').addClass('selected');
        }
        
    });
    
    $(document).ready(function(){
        
        if($('.sticky').length){
            $('h1').css('opacity','0');
            $('.puces').css('opacity','0');
            $('.description').css('opacity','0');
            $('.title-border-top').css('position','relative');
            $('.title-border-top').css('left','-1000px');
            $('.title-border-bottom').css('position','relative');
            $('.title-border-bottom').css('left','-1000px');
            setTimeout(function(){
                $('h1').animate({opacity: 1},300);
            },800);
            setTimeout(function(){
                $('.title-border-top').animate({left: '0'},300);
            },900);
            setTimeout(function(){
                $('.title-border-bottom').animate({left: '0'},300);
            },1000);
            setTimeout(function(){
                $('.description').animate({opacity: 1},300);
            },1100);
        }
        
        $('.puces a:first-child').find('.puce').addClass('selected');
    
        $('.puce').click(function(e){
            e.preventDefault();

            var id = $(this).parents('a').attr('href');
            scrollToDiv(id);

            return false;
        });
        
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();
        var videoWidth = 0;
        
        if(windowWidth >= 960 && windowWidth <= 1024){
            videoWidth = Math.floor(windowWidth-385);
        }else if(windowWidth > 1024 && windowWidth <= 1440){
            videoWidth = Math.floor(windowWidth-565);
        }else if(windowWidth > 1440){
            videoWidth = Math.floor(windowWidth-565);
        }
        var videoW = videoWidth-15;
        
        var $home = $('.content');
        var images = [];
        var counter = 0;
        var total = 0;
        
        $home
            .children()
                .find('img')
                .each(function(){ images.push(this.src); });
        total = images.length;
        preload(images)
            .done(function()
            {
                $(document)
                    .one('scroll.home', homeScroll)
                    .triggerHandler('scroll.home');
            });
        
        setTimeout(function(){

            $('.project-photo img').each(function(){
                var img = $(this);
                if(img[0].naturalHeight > img[0].naturalWidth){
                    img.css('width',videoWidth/2-15);
                }else{
                    img.css('width',videoW);
                }
            });
            if(total === 0){
                $('.puces').animate({opacity: 1},200);
            }
                    
        },1600);
        
        $('.content').css('min-height',windowHeight-100);

        function homeFadeIn ( $row )
        {
            if ( !$row.length ) return;

            var i = 0, d = $.Deferred();
            $row
                .css('display','inline-block')
                .children()
                .each(function ()
                {
                    var self = $(this);
                    if(self.attr('src')){
                        if(self[0].naturalHeight > self[0].naturalWidth){
                            self.css('width',videoWidth/2-15);
                        }else{
                            self.css('width',videoW);
                        }
                    }
                    setTimeout(function()
                    {
                        self.fadeIn(1000);
                        if (self.next().length) return;
                        counter++;
                        setTimeout(d.resolve, 200);
            
                        if(counter === total){
                            $('.puces').animate({opacity: 1},200);
                            $('.other-projects').delay(200).animate({opacity:1},200);
                        }
                        
                    }, 300 * ++i);
                });
            return d.promise();
        }

        function homeScroll ()
        {
            if ( $home.height() + $home.offset().top - 100 - $(window).scrollTop() < $(window).height())
            {
                var $row = $home.children(':hidden').first();
                if ( !$row.length ) return;
                homeFadeIn($row)
                    .done(function ()
                    {
                        $(document)
                            .one('scroll.home', homeScroll)
                            .triggerHandler('scroll.home');
                    });
            }
            else
            {
                $(document).one('scroll.home', homeScroll);
            }
        }
        
        function preload ( images )
        {
            var d = $.Deferred()
            ,   queue = images.length
            ,   update = setInterval(function ()
                {
                    var progress = Math.round((images.length - queue) / images.length * 100);
                    d.notify(progress);
                    if ( progress == 100 ) clearInterval(update);
                }, 100);

            $.each(images, function ()
            {
                $('<img>')
                    .on('load', function ()
                    {
                        if (--queue > 0) return;
                        setTimeout(d.resolve,100);
                    })
                    .attr('src', this);
            });

            return d.promise();
        }

        $home
            .children()
            .hide()
                .children()
                .hide();
        
    });
    
</script>

<?php get_footer('empty'); ?>