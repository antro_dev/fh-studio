			<!-- footer -->
			<footer>
                            <div class="footer-light">
                                <div class="center"><a href="<?php echo get_permalink(43) ?>"><span class="email">info@fh-studio.com</span></a> | <span class="tel">+1 (514) 989-PROD (7763)</span></div>
                                <div class="copy">All rights reserved - &copy; FH Studio <?php echo date('Y') ?></div>
                            </div>
                        </footer>
			<!-- /footer -->
		
		</div>
		<!-- /wrapper -->
                
                <div class="resize-your-window">
                    Please increase the size of your window to browse the website
                </div>
    
                <div id="newsletter-box">
                    <div class="newsletter-content">
                        <div class="title-border-bottom"></div>

                        <div class="title">Subscribe to<br />our newsletter</div>

                        <div class="awesome">Awesome news, awesome stuff, anyway, subscribe !</div>

                        <form name="newsletter" action="#" method="post">

                            <div class="input">
                                <input type="text" name="name" placeholder="Name" />
                            </div>

                            <div class="input">
                                <input type="text" name="email" placeholder="Email" />
                            </div>

                            <div class="submit">
                                <button>Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>

		<?php wp_footer(); ?>
		
		<!-- analytics -->
		<script>
			var _gaq=[['_setAccount','UA-XXXXXXXX-XX'],['_trackPageview']];
			(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
			g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
			s.parentNode.insertBefore(g,s)})(document,'script');
		</script>
	
	</body>
</html>