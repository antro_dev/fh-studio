<?php get_header(); ?>
	
	<!-- section -->
	<section role="main" id="talent-page">
            
            <?php 
            if (have_posts()): while (have_posts()) : 
                the_post(); 
                $biography = get_post_meta($post->ID,'wpcf-biography',TRUE); 
                $website = get_post_meta($post->ID,'wpcf-website',TRUE); 
                $citation = get_post_meta($post->ID,'wpcf-talent-citation',TRUE); 
            ?>

            <div class="banner" <?php echo (get_post_meta($post->ID,'wpcf-talent-profile-picture',TRUE))? 'style="background-image:url(\'' . get_post_meta($post->ID,'wpcf-talent-profile-picture',TRUE) . '\');"' : '' ?>>
            
                <div>
                    <div class="title-border-top"></div>

                    <h1>
                        <?php echo get_post_meta($post->ID,'wpcf-talent-first-name',TRUE) ?><br />
                        <?php echo get_post_meta($post->ID,'wpcf-talent-last-name',TRUE) ?>
                    </h1>

                    <div class="title-border-bottom"></div>
                </div>
                
            </div>

            <?php endwhile; ?>

            <?php endif; ?>
            
            <?php
                $childargs = array(
                    'post_type' => 'projects',
                    'post__not_in' => array($post->ID),
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'meta_query' => array(
                        array('key' => '_wpcf_belongs_photographer_id', 'value' => get_the_ID())
                    )
                );
                $child_posts = get_posts($childargs);
            ?>
                    
            <?php $i=1; foreach($child_posts as $child_post): ?>

            <div class="project box" style="background-image:url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($child_post->ID) ); ?>');">
                <a href="<?php echo get_permalink($child_post->ID) ?>">
                    
                </a>
            </div>

            <?php $i++; endforeach; ?>

            <?php if($i%2 != 0 || $i == 1): ; ?>

            <div class="project project-empty box biography">
                
                <h2>Biography</h2>
                
                <div class="title-border-bottom-content"></div>
                
                <div class="content">
                    <?php echo $biography ?>
                </div>
                
                <div class="website">
                    <?php if($website): ?>
                    <a href="<?php echo (strpos($website,'http') !== false)? $website : 'http://' . $website ?>" target="_blank"><?php echo $website ?></a>
                    <?php endif; ?>
                </div>
                
            </div>
            
            <?php else: ?>
            
                <div class="biography">

                    <h2>Biography</h2>
                    
                    <div class="title-border-bottom-content"></div>

                    <div class="content">
                        <?php echo $biography ?>
                    </div>

                    <div class="citation">
                        <?php if($citation): ?>
                        "<?php echo $citation ?>"
                        <?php endif; ?>
                    </div>

                    <div class="website">
                        <?php if($website): ?>
                        <a href="<?php echo (strpos($website,'http') !== false)? $website : 'http://' . $website ?>" target="_blank"><?php echo $website ?></a>
                        <?php endif; ?>
                    </div>

                </div>
            
            <?php endif; ?>
            
	
	</section>
	<!-- /section -->
	
<script type="text/javascript">
    
    
    $(document).ready(function(){
        
        if($('.banner').length){
            $('.banner').css('opacity','0');
            $('h1').css('opacity','0');
            $('h2').css('opacity','0');
            $('.title-border-top').css('position','relative');
            $('.title-border-top').css('left','-1000px');
            $('.title-border-bottom').css('position','relative');
            $('.title-border-bottom').css('left','-1000px');
            setTimeout(function(){
                $('.banner').animate({opacity: 1},300);
            },300);
            setTimeout(function(){
                $('h1').animate({opacity: 1},300);
                $('h2').animate({opacity: 1},300);
            },900);
            setTimeout(function(){
                $('.title-border-top').animate({left: '0'},300);
            },1100);
            setTimeout(function(){
                $('.title-border-bottom').animate({left: '0'},300);
            },1300);
        }
        
        var windowWidth = $(window).width();
        
        $('#talent-page .project').css('height',windowWidth/2*0.6);
        $('#talent-page .infos > div').css('width',$('#talent-page .infos').width());
        
        setTimeout(function(){
            var boxHeight = $('section > div:eq(1)').height();
            $('#talent-page .infos, #talent-page .infos > div').css('height',boxHeight-1);
        },50);
        
    });
    
</script>

<?php get_footer('empty'); ?>