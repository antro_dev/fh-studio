<?php 

    /* Template Name: Talents */ 

    get_header(); 
    
?>

<section role="main" id="talents-page" class="main">
    
    <?php
    
        if(get_the_ID() == 121){
            $types = array(
                array(
                    'type' => 'photographers',
                    'post' => 'photographer',
                    'title'=> 'Photo<br/>graph<br/>ers',
                    'h'    => 'h1'
                ),
                array(
                    'type' => 'directors',
                    'post' => 'director',
                    'title'=> 'Dir<br/>ect<br/>ors',
                    'h'    => 'h2'
                )
            );
        }else{
            $types = array(
                array(
                    'type' => 'directors',
                    'post' => 'director',
                    'title'=> 'Dir<br/>ect<br/>ors',
                    'h'    => 'h1'
                ),
                array(
                    'type' => 'photographers',
                    'post' => 'photographer',
                    'title'=> 'Photo<br/>graph<br/>ers',
                    'h'    => 'h2'
                )
            );
        }
        
        foreach($types as $type):
            $args = array(
                'post_type' => 'talents',
                'meta_query' => array(
                    array(
                        'key' => 'wpcf-talent-job',
                        'value' => $type['post'],
                        'compare' => 'LIKE'
                    )
                ),
                'meta_key' => 'wpcf-talent-last-name',
                'orderby' => 'meta_value',
                'posts_per_page' => -1,
                'order' => 'ASC',
            );
    ?>
    <?php query_posts($args); ?>
    
    <div class="<?php echo $type['type'] ?>">
        
        <div class="banner" data-speed="5" data-type="background">
            <div>
                <div class="title-border-top"></div>

                <<?php echo $type['h'] ?>>
                    <?php echo $type['title'] ?>
                </<?php echo $type['h'] ?>>

                <div class="title-border-bottom"></div>
            </div>
        </div>
        
        <?php if (have_posts()) : ?>
        
        <div class="list">
        
            <?php $i = 0; while (have_posts()) : the_post(); ?>

                <div class="talent" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>'); ">
                    <a href="<?php echo get_permalink() ?>?type=<?php echo $type['post'] ?>">
                        <h3 class="<?php echo (!get_post_meta($post->ID,'wpcf-talent-color-text',TRUE) || get_post_meta($post->ID,'wpcf-talent-color-text',TRUE) == 'light')? 'light' : 'dark'  ?>">
                            <span class="first-name"><?php echo get_post_meta($post->ID,'wpcf-talent-first-name',TRUE) ?></span><br />
                            <span class="last-name"><?php echo get_post_meta($post->ID,'wpcf-talent-last-name',TRUE) ?></span>
                        </h3>
                    </a>
                </div>

            <?php $i++; endwhile; ?>
            
            <?php if($i%2 != 0): ?>
                <div class="talent contact">
                    <a href="<?php echo get_permalink(43) ?>">
                        <div>for any inquiries,<br />take a minute<br /> to <span>contact us</span></div>
                    </a>
                </div>
            <?php endif; ?>
    
        </div>
            
        <?php endif; ?>
        
    </div>
    
    <?php endforeach; ?>

</section>

<script type="text/javascript">
    
    $(document).ready(function(){
        
        resizeTitleBorders();
        
        if($('.banner').length){
            $('.banner').css('opacity','0');
            $('h1').css('opacity','0');
            $('h2').css('opacity','0');
            $('.title-border-top').css('position','relative');
            $('.title-border-top').css('left','-1000px');
            $('.title-border-bottom').css('position','relative');
            $('.title-border-bottom').css('left','-1000px');
            setTimeout(function(){
                $('.banner').animate({opacity: 1},300);
            },300);
            setTimeout(function(){
                $('h1').animate({opacity: 1},300);
                $('h2').animate({opacity: 1},300);
            },900);
            setTimeout(function(){
                $('.title-border-top').animate({left: '0'},300);
            },1100);
            setTimeout(function(){
                $('.title-border-bottom').animate({left: '0'},300);
            },1300);
        }
        
        var $window = $(window);
        
        $('.banner[data-type="background"]').each(function(){
            var $bgobj = $(this);

            $(window).scroll(function() {
                    
                var top = $bgobj.offset().top;
                
                if($window.scrollTop()+100 >= top){
                
                    $('.banner').addClass('notransition');

                    var yPos = -(($window.scrollTop()) / $bgobj.data('speed')) + top;
                    var coords = '50% '+ yPos + 'px';

                    $bgobj.css({ backgroundPosition: coords });
                    $bgobj.css({ backgroundAttachment: 'fixed'});
                
                }else{
                    $bgobj.css({ backgroundPosition: '50% 0' });
                    $bgobj.css({ backgroundAttachment: 'scroll'});
                }

            });

        });
        
        var windowWidth = $window.width();
//        $('.talent').css('height', windowWidth/2*0.54);
        $('.talent').animate({height: windowWidth/2*0.54},2000);
//        $('.talent').mouseover(
//            function(){
//                $(this).css('background-size', windowWidth/2);
//                $(this).animate({backgroundSize: windowWidth/2+120}, 2000);
//            }
//        );
//        $('.talent').mouseout(
//            function(){
//                $(this).css({backgroundSize: windowWidth/2});
//            }
//        );
        $('.talent').hover(
            function(){
                $(this).css('background-size', windowWidth/2);
                $(this).stop(true, false).animate({backgroundSize: windowWidth/2+120}, 2000);
            },function(){
                $(this).stop(true, false).animate({backgroundSize: windowWidth/2}, 1000);
            }
        );

        $('.talent a').css('height', windowWidth/2*0.54);
        $('.talent a').css('width', windowWidth/2);
        
        $('.talent').click(function(){
            document.location.href = $(this).find('a').attr('href');
        });
        
    });

    $(window).resize(function(){
        $('.talent').css('height', $('.talent').width()*0.54);
    });
    
</script>

<?php get_footer('empty'); ?>