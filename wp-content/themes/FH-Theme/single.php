<?php get_header(); ?>
	
	<!-- section -->
	<section role="main" class="single-post" class="main">
            
            <div>
	
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    
                    <a href="<?php echo get_permalink(41) ?>" class="button back">Back to news</a>
			
                    <div class="date"><?php the_time('d.m.Y'); ?></div>

                    <h1>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                    </h1>
                    
                    <div class="title-border-bottom"></div>

                    <?php the_content(); ?>
			
			
		</article>
		<!-- /article -->
		
	<?php endwhile; ?>
	
	<?php else: ?>
	
		<!-- article -->
		<article>
			
			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
			
		</article>
		<!-- /article -->
	
	<?php endif; ?>
                
            </div>
	
	</section>
	<!-- /section -->
	