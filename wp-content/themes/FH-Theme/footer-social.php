        <footer class="footer" role="contentinfo">
            
            <div class="title-border-bottom"></div>

            <div class="do-not-miss">Don't miss a thing</div>
            
            <div class="join">Join our social networks</div>
            
            <div class="social">
                
                <div class="fb"><a href="http://facebook.com/fhstudio" target="_blank"></a></div>
                <div class="twitter"><a href="http://twitter.com/fhstudi0" target="_blank"></a></div>
                <div class="google"><a href="https://plus.google.com/u/0/118415901564587239762" target="_blank"></a></div>
                <div class="pinterest"><a href="http://www.pinterest.com/fhstudi0" target="_blank"></a></div>
                <div class="be"><a href="http://www.behance.net/fhstudio" target="_blank"></a></div>
                <div class="linkedin"><a href="http://www.linkedin.com/company/fh-studio" target="_blank"></a></div>
            </div>

        </footer>
    </div>
    
    
    <div class="resize-your-window">
        Please increase the size of your window to browse the website
    </div>
    
    <div id="newsletter-box">
        <div class="newsletter-content">
            <div class="title-border-bottom"></div>
            
            <div class="title">Subscribe to<br />our newsletter</div>
            
            <div class="awesome">Awesome news, awesome stuff, anyway, subscribe !</div>
            
            <form name="newsletter" action="#" method="post">
                
                <div class="input">
                    <input type="text" name="name" placeholder="Name" />
                </div>
                
                <div class="input">
                    <input type="text" name="email" placeholder="Email" />
                </div>
                    
                <div class="submit">
                    <button>Subscribe</button>
                </div>
            </form>
        </div>
    </div>

</body>

</html>