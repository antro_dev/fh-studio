<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	
    <!-- article -->
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <?php if ( has_post_thumbnail()) : ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <?php the_post_thumbnail(array(450,270)); ?>
                    </a>
            <?php endif; ?>

            <div class="date"><?php the_time('d.m.Y'); ?></div>

            <h2>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
            </h2>
            
            <div class="title-border-bottom"></div>

            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="button">Read Full Article</a>

    </article>
    <!-- /article -->
	
<?php endwhile; ?>

<?php else: ?>

    <!-- article -->
    <article>
            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
    </article>
    <!-- /article -->

<?php endif; ?>

<script type="text/javascript">
    
    $(document).ready(function(){
        
        var postWidth = $('.post').width();
        $('.post .wp-post-image').css('width',postWidth);
        
        $('.post').mouseover(function(){
            $(this).css('color','#fff');
            $(this).find('h2 a').css('color','#fff');
            $(this).find('.title-border-bottom').css('background-color','#fff');
            $(this).find('.button').css('background-color','#fff');
            $(this).find('.button').css('background-image','url("<?php bloginfo('template_directory'); ?>/img/icon-btn-post-hover.png")');
            $(this).find('.button').css('color','#f01776');
            $(this).find('.button').css('border-bottom-color','#c0125e');
        });
        $('.post').mouseleave(function(){
            $(this).css('color','#3a373a');
            $(this).find('h2 a').css('color','#3a373a');
            $(this).find('.title-border-bottom').css('background-color','#ed2977');
            $(this).find('.button').css('background-color','#f01776');
            $(this).find('.button').css('background-image','url("<?php bloginfo('template_directory'); ?>/img/icon-btn-post.png")');
            $(this).find('.button').css('color','#fff');
            $(this).find('.button').css('border-bottom-color','#cccccc');
        });
        
    });

    var $container = $('.posts');
    
//    $container.isotope({
//        itemSelector : '.post',
//        layoutMode: 'masonry',
//        masonry : {
//          columnWidth : 520
//        },
//    });

</script>
