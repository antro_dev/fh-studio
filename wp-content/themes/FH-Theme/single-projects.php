<?php get_header(); ?>
	
	<!-- section -->
	<section role="main" id="project-page" class="main">
            
            <div>
	
                <div class="infos">
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                        <?php $talent_id = wpcf_pr_post_get_belongs($post->ID, 'talents') ?>

                        <div class="sticky">

                            <div class="title-border-bottom"></div>

                            <h1>
                                <?php the_title(); ?>
                            </h1>

                            <?php if($director = get_post_meta($post->ID,'wpcf-project-director', true)): ?>

                            <div class="director details"><span>Director:</span> <?php echo $director ?></div>

                            <?php endif; ?>

                            <?php if($dop = get_post_meta($post->ID,'wpcf-project-dop', true)): ?>

                            <div class="photographer details"><span>Photographer:</span> <?php echo $dop ?></div>

                            <?php endif; ?>

                            <div class="description"><?php the_content() ?></div>

                            <div class="puces">

                                <?php $i=1; foreach(get_post_meta($post->ID,'wpcf-videos') as $video): if($video): ?>

                                <a href="#video-<?php echo $i ?>"><div class="puce"></div></a>

                                <?php $i++; endif; endforeach; ?>

                                <?php $i=1; foreach(get_post_meta($post->ID,'wpcf-photos') as $photo): if($photo): ?>

                                <a href="#photo-<?php echo $i ?>"><div class="puce"></div></a>

                                <?php $i++; endif; endforeach; ?>

                            </div>

                        </div>

                    <?php endwhile; ?>

                    <?php endif; ?>
                </div>

                <div class="content">

                    <?php $i=1; foreach(get_post_meta($post->ID,'wpcf-photos') as $photo): if($photo): ?>

                    <div class="project-photo project" id="photo-<?php echo $i ?>" data-project-id="<?php echo $post->ID ?>" data-project-type="video" data-project-content="<?php echo $photo ?>">
                        <div class="tools">
                            <div class="bookmark">Bookmark</div>
                            <div class="facebook"></div>
                            <div class="twitter"></div>
                            <div class="google"></div>
                            <div class="pinterest"></div>
                        </div>
                        <img src="<?php echo $photo ?>" />
                    </div>

                    <?php $i++; endif; endforeach; ?>

                </div>
                <?php if($talent_id): ?>
                <?php
                    $childargs = array(
                        'post_type' => 'projects',
                        'post__not_in' => array($post->ID),
                        'orderby' => 'post_date',
                        'order' => 'DESC',
                        'posts_per_page' => 3,
                        'meta_query' => array(
                            'relation' => 'and',
                            array('key' => '_wpcf_belongs_talents_id', 'value' => $talent_id),
                            array(
                                'key' => 'wpcf-talent-featured-project',
                                'value' => 1,
                                'compare' => 'is',
                            ),
                        )
                    );
                    $child_posts = get_posts($childargs);
                ?>

                <div class="other-projects">
                    <div class="left">
                        <div>
                            <?php echo get_the_title($talent_id) ?><br />
                            Other<br />
                            Projects
                        </div>
                    </div>

                    <div class="more project-container">

                        <ul>

                        <?php $i=0; foreach($child_posts as $child_post): ?>

                        <li class="project box">
                            <a class="normal" href="<?php echo get_permalink($child_post->ID) ?>" style="background-image:url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($child_post->ID) ); ?>');">
                                <div<?php echo (get_post_meta($post->ID,'wpcf-type',TRUE) == 'video' || get_post_meta($post->ID,'wpcf-type',TRUE) == 'both')? ' class="video"' : '' ?>>
                                    <div>
                                        <h3><?php echo get_the_title($child_post->ID); ?></h3>
                                    </div>
                                </div>
                            </a>
                            <div class="background"></div>
                        </li>

                        <?php $i++; endforeach; ?>

                        <?php if($i%3 != 0 || $i == 0): $modulo_citation = $i%3; endif; ?> 

                            <li class="project citation-box">
                                <div class="citation">

                                    <?php echo get_post_meta($parent_director_id,'wpcf-citation',TRUE) ?>

                                    <span><?php echo (get_post_meta($parent_director_id,'wpcf-citation-by',TRUE) != '')? '<br /><br />' . get_post_meta($parent_director_id,'wpcf-citation-by',TRUE) : '' ?></span>
                                </div>
                            </li>

                        </ul>

                    </div>
                </div>
                <?php endif; ?>
                
            </div>
	
	</section>
	<!-- /section -->
	
<!--<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.unveil.min.js"></script>-->
<script type="text/javascript">
    
    var idPuce;
    var stickyH = $('.sticky').height()+120;
    var divProjectsTop = 0;
    
    $(window).scroll(function(){
        var top = $(this).scrollTop();
        var otherTop  = $('.other-projects').offset().top;
        
        if(otherTop <= top+stickyH){
            
//            $('.other-projects').delay(500).animate({opacity:1},400);
            
        }else if(otherTop > top){
            var newTop = top+100;
            $('.sticky').css({top: newTop});
        }
        $('.project-photo, .project-video').each(function(){
            var dataTop = $(this).offset().top;
            if(dataTop <= top+105){
                idPuce = $(this).attr('id');
            }
        });
        
        if(idPuce){
            $('.puce').removeClass('selected');
            $('a[href="#' + idPuce + '"] > div').addClass('selected');
        }
        
    });
    
    $(document).ready(function(){

        addRollover();
        
        if($('.sticky').length){
            $('h1').css('opacity','0');
            $('.director').css('opacity','0');
            $('.photographer').css('opacity','0');
            $('.puces').css('opacity','0');
            $('.description').css('opacity','0');
            $('.title-border-top').css('position','relative');
            $('.title-border-top').css('left','-1000px');
            $('.title-border-bottom').css('position','relative');
            $('.title-border-bottom').css('left','-1000px');
            setTimeout(function(){
                $('h1').animate({opacity: 1},300);
            },800);
            setTimeout(function(){
                $('.title-border-top').animate({left: '0'},300);
            },900);
            setTimeout(function(){
                $('.title-border-bottom').animate({left: '0'},300);
            },1000);
            setTimeout(function(){
                $('.director').animate({opacity: 1},300);
            },1100);
            setTimeout(function(){
                $('.photographer').animate({opacity: 1},300);
            },1200);
            setTimeout(function(){
                $('.description').animate({opacity: 1},300);
            },1300);
        }
        
        $('.puces a:first-child').find('.puce').addClass('selected');
    
        $('.puce').click(function(e){
            e.preventDefault();

            var id = $(this).parents('a').attr('href');
            scrollToDiv(id);

            return false;
        });
        
        $('.project-container .project').mouseover(function(){
            $(this).find('a h3').css('color', '#f01776');
        }).mouseleave(function(){
            $(this).find('a h3').css('color', '#373437');
        });
        
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();
        var videoWidth = 0;
        var othersWidth = Math.floor(windowWidth-385);
        var smallProject = othersWidth/3;
        
        if(windowWidth >= 960 && windowWidth <= 1024){
            videoWidth = Math.floor(windowWidth-385);
            smallProject = othersWidth/2;
            if(<?php echo $i ?>%2 === 0 || <?php echo $i ?> === 0){
                $('.citation-box').css('display','none');
                $('.other-projects .more .citation-box').css('width', (smallProject-20));
                $('.other-projects .more .citation').css('width', (smallProject-20-75));
            }
        }else if(windowWidth > 1024 && windowWidth <= 1440){
            videoWidth = Math.floor(windowWidth-565);
            if(<?php echo $i ?>%3 === 0 || <?php echo $i ?> === 0){
                $('.citation-box').css('display','none');
                $('.other-projects .more .citation-box').css('width', (smallProject-20)*<?php echo ($modulo_citation == 1)? 2 : 1; ?>);
                $('.other-projects .more .citation').css('width', (smallProject-20-75)*<?php echo ($modulo_citation == 1)? 2 : 1; ?>);
            }
        }else if(windowWidth > 1440){
            videoWidth = Math.floor(windowWidth-565);
        }
        var videoW = videoWidth-15;
        var videoHeight = videoWidth*0.57-15;
        var strVideos = '';
        
        <?php 
            $i=1; 
            foreach(get_post_meta($post->ID,'wpcf-videos') as $video): 
                if($video != ''):
        ?>
        
        strVideos += '<div id="video-<?php echo $i ?> project" class="project-video" data-project-id="<?php echo $post->ID ?>" data-project-type="video" data-project-content="<?php echo $video ?>">';
        strVideos += '<div class="tools"><div class="bookmark">Bookmark</div><div class="facebook"></div><div class="twitter"></div><div class="google"></div><div class="pinterest"></div></div>';
        strVideos += '<iframe src="http://player.vimeo.com/video/<?php echo $video ?>?color=f01776" width="' + videoW + '" height="' + videoHeight + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
        strVideos += '</div>';
        
        <?php 
                    $i++;
                endif;
            endforeach; 
        ?>
            
        $('.content').prepend(strVideos);
        
        var $home = $('.content');
        var images = [];
        var counter = 0;
        var total = 0;
        var counterVid = 1;
        
        $home
            .children()
                .find('img')
                .each(function(){ images.push(this.src); });
        total = images.length;
        preload(images)
            .done(function()
            {
                $(document)
                    .one('scroll.home', homeScroll)
                    .triggerHandler('scroll.home');
            });
        
        setTimeout(function(){

            $home
                .children('.project-video')
                    .each(function(){
                        var $vid = $(this);
                        $vid.css('display','inline-block');
                        $vid.find('.tools').css('display','block');
                        $vid.find('iframe').delay(counterVid*200).fadeIn(500);
                        counterVid++;
                    });
            $('.project-photo img').each(function(){
                var img = $(this);
                if(img[0].naturalHeight > img[0].naturalWidth){
                    img.css('width',videoWidth/2-15);
                }else{
                    img.css('width',videoW);
                }
            });
            if(total === 0){
                $('.puces').animate({opacity: 1},200);
                $('.other-projects').delay(200).animate({opacity:1},200);
            }
                    
        },1600);
        
        $('.content').css('min-height',windowHeight-100);
        $('.other-projects .more').css('width', othersWidth);
        $('.other-projects .more .project').css('width', smallProject);
        $('.other-projects .more .project').css('height', smallProject*0.65);
        $('.other-projects .left > div').css('height', smallProject*0.65);
        
        $('.project-photo, .project-video').hover(function(){
            $(this).find('.tools').animate({opacity: 1}, 200);
        }).mouseleave(function(){
            $(this).find('.tools').animate({ opacity: 0}, 200);
        });

        function homeFadeIn ( $row )
        {
            if ( !$row.length ) return;

            var i = 0, d = $.Deferred();
            $row
                .css('display','inline-block')
                .children()
                .each(function ()
                {
                    var self = $(this);
                    if(self.attr('src')){
                        if(self[0].naturalHeight > self[0].naturalWidth){
                            self.css('width',videoWidth/2-15);
                        }else{
                            self.css('width',videoW);
                        }
                    }
                    setTimeout(function()
                    {
                        self.fadeIn(1000);
                        if (self.next().length) return;
                        counter++;
                        setTimeout(d.resolve, 200);
            
                        if(counter === total){
                            $('.puces').animate({opacity: 1},200);
                            $('.other-projects').delay(200).animate({opacity:1},200);
                        }
                        
                    }, 300 * ++i);
                });
            return d.promise();
        }

        function homeScroll ()
        {
            if ( $home.height() + $home.offset().top - 100 - $(window).scrollTop() < $(window).height())
            {
                var $row = $home.children(':hidden').first();
                if ( !$row.length ) return;
                homeFadeIn($row)
                    .done(function ()
                    {
                        $(document)
                            .one('scroll.home', homeScroll)
                            .triggerHandler('scroll.home');
                    });
            }
            else
            {
                $(document).one('scroll.home', homeScroll);
            }
        }
        
        function preload ( images )
        {
            var d = $.Deferred()
            ,   queue = images.length
            ,   update = setInterval(function ()
                {
                    var progress = Math.round((images.length - queue) / images.length * 100);
                    d.notify(progress);
                    if ( progress == 100 ) clearInterval(update);
                }, 100);

            $.each(images, function ()
            {
                $('<img>')
                    .on('load', function ()
                    {
                        if (--queue > 0) return;
                        setTimeout(d.resolve,100);
                    })
                    .attr('src', this);
            });

            return d.promise();
        }

        $home
            .children()
            .hide()
                .children()
                .hide();
        
    });
    
</script>

<?php get_footer('empty'); ?>