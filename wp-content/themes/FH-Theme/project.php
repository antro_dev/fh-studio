<?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post(); ?>

    <?php $parent_talent_id = wpcf_pr_post_get_belongs($post->ID, 'talents') ?>
    <?php $tags = get_post_meta($post->ID,'wpcf-project-tag') ?>

    <li class="project new <?php echo (get_post_meta($post->ID,'wpcf-type',TRUE) && get_post_meta($post->ID,'wpcf-type',TRUE) != 'both')? 'tag-' . get_post_meta($post->ID,'wpcf-type',TRUE) : 'tag-video tag-photo' ?><?php foreach($tags[0] as $tag) echo strtolower(' tag-' . $tag); ?>">
        <a class="normal" href="<?php echo get_permalink() ?>" style=" background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>');">

            <div<?php echo (get_post_meta($post->ID,'wpcf-type',TRUE) == 'video')? ' class="video"' : '' ?>>
                <div>
                    <h3><?php the_title(); ?></h3>
                    <span><?php echo get_the_title(($parent_talent_id)? $parent_talent_id : 'Production') ?></span>
                </div>
            </div>

        </a>
        <div class="background"></div>
    </li>

    <?php endwhile; ?>

<?php endif; ?>