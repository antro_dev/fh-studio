<?php 

    /* Template Name: e-com photo */ 

    get_header(); 
    
?>

<section role="main" id="ecom-page" class="main">
        
    <div class="banner" data-speed="5" data-type="background">
        <div>
            <div class="title-border-top"></div>

            <h1>
                E-Com<br/>photo</br>graphy
            </h1>

            <div class="title-border-bottom"></div>
        </div>
    </div>

    <div class="service sales">
        <div class="title-border-bottom"></div>
        <h2>Generate <br/>sales <br/><span>FASTER</span></h2>
        <div class="description">We pick up the goods, shoot and upload it directly to your secure FTP, Via drop Box or others after 24H only.</div>
    </div>

    <div class="service quality">
        <div class="title-border-bottom"></div>
        <h2>keep <span>high <br/>quality</span> & <br/>consistency</h2>
        <div class="description">Our technology using robots make sure to keep the same level of quality across all your pictures.</div>
    </div>
    
    <div class="service quantity">
        <div class="title-border-bottom"></div>
        <h2>shoot <br/>high <br/><span>quantity</span></h2>
        <div class="description">Using our factory process and technology we could shoot up to 200 skus per day per client means 600 images daily.</div>
    </div>
    
    <div class="service cost">
        <div class="title-border-bottom"></div>
        <h2>keep cost <br/>very <br/><span>low</span></h2>
        <div class="description">We guarantee to offer the best yet the lowest cost on the market, if you find a lower price with same quality, we will beat it by 10%. 

        <br /><br />* All models fees are included</div>
    </div>
    
    <div class="process">
        
        <div>
            <div class="title-border-bottom"></div>
            <h2>The Process</h2>

            <div class="line"></div>

            <div class="step clock">
                <div class="image"></div>
                <div class="bullet"></div>
                <div class="text">
                    You select a day
                    and time for a pick up 
                    (minimum 60 skus)
                </div>
            </div>

            <div class="step truck">
                <div class="image"></div>
                <div class="bullet"></div>
                <div class="text">
                    We pick up the
                    goods and the truck
                    arrives at FH studios
                </div>
            </div>

            <div class="step box">
                <div class="image"></div>
                <div class="bullet"></div>
                <div class="text">
                    Check all the 
                    products taken
                    and prepare them.
                </div>
            </div>

            <div class="step photo">
                <div class="image"></div>
                <div class="bullet"></div>
                <div class="text">
                    Photoshoot 
                    process
                </div>
            </div>

            <div class="step truck2">
                <div class="image"></div>
                <div class="bullet"></div>
                <div class="text">
                    Goods are shipped
                    back to your 
                    warehouse
                </div>
            </div>

            <div class="step computer">
                <div class="image"></div>
                <div class="bullet"></div>
                <div class="text">
                    Post production 
                    process
                </div>
            </div>

            <div class="step download">
                <div class="image"></div>
                <div class="bullet"></div>
                <div class="text">
                    Delivery via 
                    drop box, 
                    wetransfer 
                    or FTP
                </div>
            </div>
        </div>
        
    </div>
    
    <?php
        $args = array(
            'post_type' => 'ecom-work',
            'orderby' => 'title',
            'order' => 'ASC',
        );
    ?>
    <?php query_posts($args); ?>

    <div class="works">
        
        <div>
        
            <div class="title-border-bottom"></div>

            <h2>Our Work</h2>
            
        </div>

    </div>

    <?php if (have_posts()) : ?>

    <div class="workslist">
        <?php while (have_posts()) : the_post(); ?>

        <div style="background-image: url('<?php echo get_post_meta($post->ID,'wpcf-ecom-image',TRUE); ?>');"></div>

        <?php endwhile; ?>
    </div>

    <?php endif; ?>
    
    <div class="quote">
        
        <div>
        
            <div class="title-border-bottom"></div>

            <h2>Ask for a quote</h2>

            <div class="citation">
                <p>“FH Studio’s E-com Photography was one of the best experience dealing with contractants”</p>
                <span>_John Stoname - CEO of RONA</span>
            </div>

            <form action="#" method="post" name="ecom-request">

                <div class="col">
                    <div class="input">
                        <input type="text" name="company" placeholder="Company name *" />
                    </div>

                    <div class="input">
                        <input type="text" name="name" placeholder="Name *" />
                    </div>

                    <div class="input">
                        <input type="text" name="email" placeholder="Email *" />
                    </div>

                    <div class="input">
                        <input type="text" name="tel" placeholder="Company phone *" />
                    </div>

                    <input type="checkbox" name="newsletter" id="newsletter" /><label class="newsletter" for="newsletter">Subscribe to our newsletter</label>

                    <span class="required">* Required fields</span>

                </div>

                <div class="col right">

                    <div class="input">
                        <select name="kind-of-set-up">
                            <option value="">Kind of Set up*</option>
                            <option value="Models">Models</option>
                            <option value="Table-top">Table top</option>
                            <option value="Dummy">Dummy</option>
                        </select>
                    </div>

                    <div class="input">
                        <select name="type-of-products">
                            <option value="">Type of Products*</option>
                            <option value="Clothing">Clothing</option>
                            <option value="Outerwear">Outerwear</option>
                            <option value="Lingerie">Lingerie</option>
                            <option value="Eye-wear">Eye-wear</option>
                            <option value="Footwear">Footwear</option>
                            <option value="Bags">Bags</option>
                            <option value="Belts">Belts</option>
                            <option value="Scarves">Scarves</option>
                            <option value="Hats">Hats</option>
                            <option value="Gloves">Gloves</option>
                            <option value="Wallets">Wallets</option>
                            <option value="Accessories">Accessories</option>
                            <option value="Jewelry">Jewelry</option>
                            <option value="Others">Others</option>
                        </select>
                    </div>

                    <div class="input">
                        <select name="skus">
                            <option value="">Amount of SKUs / year*</option>
                            <option value="50-to-100">50 to 100</option>
                            <option value="101-to-500">101 to 500</option>
                            <option value="501-to-1000">501 to 1000</option>
                            <option value="1001-to-1500">1001 to 1500</option>
                            <option value="1501-to-3000">1501 to 3000</option>
                            <option value="3001-to-5000">3001 to 5000</option>
                            <option value="5001-and-more">5001 and more</option>
                        </select>
                    </div>

                    <div class="input">
                        <input type="text" name="more" placeholder="Precisions... *" />
                    </div>
                </div>

                <div class="input-btn">
                    <button type="button">Send</button>
                </div>

            </form>
        </div>
        
    </div>
    
</section>

<?php get_footer('empty'); ?>

<script type="text/javascript">
    
    $(document).ready(function(){
        
        resizeTitleBorders();
        
        if($('.banner').length){
            $('.banner').css('opacity','0');
            $('h1').css('opacity','0');
            $('h2').css('opacity','0');
            $('.title-border-top').css('position','relative');
            $('.title-border-top').css('left','-1000px');
            $('.title-border-bottom').css('position','relative');
            $('.title-border-bottom').css('left','-1000px');
            $('.process .line').css('width','0');
            $('.process .step .image').css('opacity','0');
            $('.process .step .text').css('opacity','0');
            $('.process .step .bullet').css({backgroundSize:'0'});
            setTimeout(function(){
                $('.banner').animate({opacity: 1},300);
            },300);
            setTimeout(function(){
                $('h1').animate({opacity: 1},300);
                $('h2').animate({opacity: 1},300);
            },900);
            setTimeout(function(){
                $('.title-border-top').animate({left: '0'},300);
            },1100);
            setTimeout(function(){
                $('.title-border-bottom').animate({left: '0'},300);
            },1300);
        }
        
        var $window = $(window);
        
        $('.banner[data-type="background"]').each(function(){
            var $bgobj = $(this);

            $window.scroll(function() {
                    
                var top = $bgobj.offset().top;
                
                if($window.scrollTop()+100 >= top){
                
                    $('.banner').addClass('notransition');

                    var yPos = -($window.scrollTop() / $bgobj.data('speed')) + top;
                    var coords = '50% '+ yPos + 'px';

                    $bgobj.css({ backgroundPosition: coords });
                    $bgobj.css({ backgroundAttachment: 'fixed'});
                
                }else{
                    $bgobj.css({ backgroundPosition: '50% 0' });
                    $bgobj.css({ backgroundAttachment: 'scroll'});
                }

            });

        });
        
        var $processTop = $('.process').offset().top;
        var start = false;
        
        $window.scroll(function() {
            if(($processTop-$window.height()+300 < $window.scrollTop()) && start === false){
                start = true;
                
                $('.process .clock .image').animate({opacity: '1'},500);
                $('.process .clock .text').animate({opacity: '1'},500);
                $('.process .clock .bullet').animate({backgroundSize:'100%'},500);
                
                if(windowWidth >= 960 && windowWidth <= 1024){
                    $('.process .line').animate({width: '660px'},5000);
                }else if(windowWidth > 1025 && windowWidth <= 1440){
                    $('.process .line').animate({width: '920px'},5000);
                }else if(windowWidth > 1440){
                    $('.process .line').animate({width: '920px'},5000);
                }

                setTimeout(function(){
                    $('.process .truck .image').animate({opacity: '1'},500);
                    $('.process .truck .text').animate({opacity: '1'},500);
                    $('.process .truck .bullet').animate({backgroundSize:'100%'},500);
                },600);
                setTimeout(function(){
                    $('.process .box .image').animate({opacity: '1'},500);
                    $('.process .box .text').animate({opacity: '1'},500);
                    $('.process .box .bullet').animate({backgroundSize:'100%'},500);
                },1200);
                setTimeout(function(){
                    $('.process .photo .image').animate({opacity: '1'},500);
                    $('.process .photo .text').animate({opacity: '1'},500);
                    $('.process .photo .bullet').animate({backgroundSize:'100%'},500);
                },1800);
                setTimeout(function(){
                    $('.process .truck2 .image').animate({opacity: '1'},500);
                    $('.process .truck2 .text').animate({opacity: '1'},500);
                    $('.process .truck2 .bullet').animate({backgroundSize:'100%'},500);
                },2400);
                setTimeout(function(){
                    $('.process .computer .image').animate({opacity: '1'},500);
                    $('.process .computer .text').animate({opacity: '1'},500);
                    $('.process .computer .bullet').animate({backgroundSize:'100%'},500);
                },3000);
                setTimeout(function(){
                    $('.process .download .image').animate({opacity: '1'},500);
                    $('.process .download .text').animate({opacity: '1'},500);
                    $('.process .download .bullet').animate({backgroundSize:'100%'},500);
                },3600);
            }
        });
        
        var windowWidth = $window.width();
//        $('#ecom-page .service').css('height',windowWidth/2*0.85);
        
    });
    
</script>