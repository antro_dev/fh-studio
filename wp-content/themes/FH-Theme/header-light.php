<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
		
		<!-- dns prefetch -->
		<link href="//www.google-analytics.com" rel="dns-prefetch">
		
		<!-- meta -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		
		<!-- icons -->
		<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon@x2.png" />
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/icon_128x128.png" rel="apple-touch-icon-precomposed">
                	
		<!-- css + javascript -->
		<?php wp_head(); ?>
                
                <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/home-slider.css" />
                <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/home-script.js"></script>
                <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.86080.js"></script>
                
		<script>
		!function(){
			// configure legacy, retina, touch requirements @ conditionizr.com
			conditionizr()
		}()
		</script>
	</head>
	<body <?php body_class(); ?>>
	
		<!-- wrapper -->
		<div class="wrapper">