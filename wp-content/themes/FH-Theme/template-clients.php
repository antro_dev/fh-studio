<?php 

    /* Template Name: Clients */ 

    get_header(); 
    
?>

<section role="main" id="company-page" class="main">
    
    <?php
        $args = array(
            'post_type' => 'clients',
            'orderby' => 'title',
            'order' => 'ASC',
        );
    ?>
    <?php query_posts($args); ?>
    
    <div class="clients">
        
        <div class="banner" data-speed="5" data-type="background">
            <div>
                    <div class="title-border-top"></div>
                
                <h2>
                    Cli<br/>en</br>ts
                </h2>

                <div class="title-border-bottom"></div>
            </div>
        </div>
        
        <?php if (have_posts()) : ?>
        
        <div class="list">
        
            <?php while (have_posts()) : the_post(); ?>

            <div>
                <div class="client" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>'); ">
                    <a href="<?php echo (get_post_meta($post->ID,'wpcf-client-website',TRUE))? 'http://' . get_post_meta($post->ID,'wpcf-client-website',TRUE) : ''; ?>" target="_blank">

                    </a>
                </div>
            </div>

            <?php endwhile; ?>
    
        </div>
            
        <?php endif; ?>
        
    </div>
    
    <?php
        $post = get_page(131); 
        $content = apply_filters('the_content', $post->post_content);
    ?>
	
    <div class="banner" data-speed="5" data-type="background">
        <div>
            <div class="title-border-top"></div>

            <h1>
                Ab<br/>out</br>us
            </h1>

            <div class="title-border-bottom"></div>
        </div>
    </div>

    <article id="post-131">

        <div class="title-border-bottom"></div>

        <div class="content">
            <?php echo $content; ?>
        </div>

        <div class="fancy-logo"></div>

    </article>
    <!-- /article -->

</section>

<script type="text/javascript">
    
    $(document).ready(function(){
        
        resizeTitleBorders();
        
        if($('.banner').length){
            $('.banner').css('opacity','0');
            $('h1').css('opacity','0');
            $('h2').css('opacity','0');
            $('.title-border-top').css('position','relative');
            $('.title-border-top').css('left','-1000px');
            $('.title-border-bottom').css('position','relative');
            $('.title-border-bottom').css('left','-1000px');
            setTimeout(function(){
                $('.banner').animate({opacity: 1},300);
            },300);
            setTimeout(function(){
                $('h1').animate({opacity: 1},300);
                $('h2').animate({opacity: 1},300);
            },900);
            setTimeout(function(){
                $('.title-border-top').animate({left: '0'},300);
            },1100);
            setTimeout(function(){
                $('.title-border-bottom').animate({left: '0'},300);
            },1300);
        }
        
        var $window = $(window);
        
        $('.banner[data-type="background"]').each(function(){
            var $bgobj = $(this);

            $(window).scroll(function() {
                    
                var top = $bgobj.offset().top;
                
                if($window.scrollTop()+100 >= top){
                
                    $('.banner').addClass('notransition');

                    var yPos = -($window.scrollTop() / $bgobj.data('speed')) + top;
                    var coords = '50% '+ yPos + 'px';

                    $bgobj.css({ backgroundPosition: coords });
                    $bgobj.css({ backgroundAttachment: 'fixed'});
                
                }else{
                    $bgobj.css({ backgroundPosition: '50% 0' });
                    $bgobj.css({ backgroundAttachment: 'scroll'});
                }

            });

        });
        
        var contentLeft = $('article .content').offset().left;
        $('article .title-border-bottom').css('margin-left',contentLeft);
        
        var contentHeight = $('article').height();
        $('.fancy-logo').css('height',contentHeight);
        
        var windowWidth = $window.width();
        if(windowWidth >= 960 && windowWidth <= 1024){
            $('.clients .client').css('width', windowWidth/4);
            $('.clients .client').css('height', windowWidth/4);
        }else if(windowWidth > 1025 && windowWidth <= 1440){
            $('.clients .client').css('width', windowWidth/6);
            $('.clients .client').css('height', windowWidth/6);
        }else if(windowWidth > 1440){
            $('.clients .client').css('width', windowWidth/6);
            $('.clients .client').css('height', windowWidth/6);
        }
        
    });
    
</script>

<?php get_footer('empty'); ?>