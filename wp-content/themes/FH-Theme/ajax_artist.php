<div class="title-border-bottom"></div>

<h2>Artists</h2>


<?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post(); ?>

        <?php $job = get_post_meta($post->ID,'wpcf-talent-job',TRUE) ?>

    <div class="artist">
        <div>
            <h3><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></h3>
            <span><?php echo (count($job) > 1)? implode($job,' + ') : $job; ?></span>
        </div>
    </div>

    <?php endwhile; ?>

<?php else: ?>

<p>There is no artist related to your research.</p>

<?php endif; ?>