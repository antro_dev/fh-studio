var baseUrl = 'http://fh-studio.dev';
//var baseUrl = 'http://widtech.info/fh-studio';
var loaded = false;
direction = -1;
currentBlockPos = { x: -1, y: -1 }
previousBlockPos = { x: -1, y: -1 }

$(document).ready(function(){

    var supports3DTransforms =  document.body.style['webkitPerspective'] !== undefined ||
        document.body.style['MozPerspective'] !== undefined;
        $.cookie.json = true;

    linkify( '#menu-main li' );
    
    /**
     * DROPDOWN SELECT STYLED
     **/  
    
    $('select').fancySelect();
    
    /**
     * PROJECT HOVER EFFECT
     **/  

    $("header").on("mouseenter", function(){
        currentBlockPos.y = -1;
        var selected = $('.project-container  .selected');
        removeAllClasses(selected);
        selected.removeClass("selected");
    });

    $("footer").on("mouseenter", function(){
        currentBlockPos.y = $( document ).height();
        var selected = $('.project-container  .selected');
        removeAllClasses(selected);
        selected.removeClass("selected");
    });

    setUpHoverEffect();
    
    
    /**
     * TAGS FILTERS
     **/  

    $container = $('.production .project-container ul');

    var selectorType = '*';
    var selectorCategory = '*';

    $('#content .filter-type a').click(function(){
        $('#content .filter-type a').removeClass('selected');
        $(this).addClass('selected');
        selectorType = $(this).attr('data-filter');
        if(selectorType === '*'){
            $container.find('li:hidden').each(function(){
                $(this).show('scale',1000);
            });
        }else{

            $container.find('li:not(' + selectorType + ')').hide("scale", 1000, function(){
                $container.find('li' + selectorType + ':hidden').show('scale',1000);
            });

        }
        return false;
    });

    $('#content .filter-category a').click(function(){
        $('#content .filter-category a').removeClass('selected');
        $(this).addClass('selected');
        selectorCategory = $(this).attr('data-filter');
        if(selectorCategory === '*'){
            $container.find('li:hidden').each(function(){
                $(this).show('scale',1000);
            });
        }else{

            $container.find('li:not(' + selectorCategory + ')').hide("scale", 1000, function(){
                $container.find('li' + selectorCategory + ':hidden').show('scale',1000);
            });

        }
        return false;
    });

    /**
     * MENU EFFECTS
     **/  
    function linkify( selector ) {
        if( supports3DTransforms ) {

            $(selector).each(function(){
                var url = $(this).find('a').attr('href');
                var text = $(this).find('a').html();
                var submenu = $(this).find('ul').html();
                if(submenu){
                    submenu = '<ul class="sub-menu">' + submenu +'</ul>';
                    $(this).html('<div class="roll"><div><a href="' + url + '" class="s">' + text + '</a></div><div><a href="' + url + '" class="s">' + text + '</a></div></div>' + submenu);
                }else{
                    submenu = '';
                    $(this).html('<div class="roll"><div><a href="' + url + '">' + text + '</a></div><div><a href="' + url + '">' + text + '</a></div></div>');
                }
                
                $(this).click(function(){
                    var url = $(this).find('div:first-child a').attr('href');
                    document.location.href = url;
                });
            });
        }
    }
    
    /**
     * NEWSLETTER
     **/  
    $('.top-nav .newsletter').click(function(){
        var newsletterBox = $('#newsletter-box').html();
        $.fancybox({
            content:    newsletterBox,
            width:      700,
            height:     'auto',
            autoSize:   false,
            padding:    0
        });
    });
    
    /**
     * BOOKMARK
     **/  
    $(document).on('click', '.tools .bookmark', function(){
        var toBook = $(this);
        var projectId = toBook.parents('.project').attr('data-project-id');
        var type = toBook.parents('.project').attr('data-project-type');
        var content = toBook.parents('.project').attr('data-project-content');
        
        if(!$.cookie("bookmark")){
            setCookie(toBook,projectId,type,content,false);
        }else{
            var cookieId = $.cookie("bookmark");
            setCookie(toBook,projectId,type,content,cookieId);
        }
    });
    
    /**
     * SEARCH
     **/  
    $('.search a').click(function(e){
        e.preventDefault();
        
        $('#search-box').fadeIn('fast');
        $('#search-box input').focus();
        
    });
    
    $('#search-box .close-search').click(function(){
        $('#search-box').fadeOut('fast');
        $('#search-result').fadeOut('fast');
        $('.main').fadeIn('fast');
        $('footer').fadeIn('fast');
        $('#search-box').css('position','fixed');
        $('#search-box, #search-result').css('opacity','0.96');
        $('#search-input').val('');
    });
    
    $('#search-box input[name="type"]').click(function(){
        $('#search-box .type label').removeClass('selected');
        $(this).next('label').addClass('selected');
    });
    
    $('#search-result').css('min-height',$(window).height()-500);
    
    $('#search-box .type label').click(function(){
        setTimeout(function(){
            type = $('#search-box input[name="type"]:checked').val();
            valSearch = $('#search-input').val();
            if(valSearch != ''){
                searchAjax(valSearch,type); 
            }
        },20);
    });
    
    $.event.special.inputchange = {
        setup: function() {
            var self = this, val;
                $.data(this, 'timer', window.setInterval(function() {
                    val = self.value;
                    if ( $.data( self, 'cache') != val ) {
                        $.data( self, 'cache', val );
                        $( self ).trigger( 'inputchange' );
                    }
                }, 20));
            },
        teardown: function() {
            window.clearInterval( $.data(this, 'timer') );
        },
        add: function() {
            $.data(this, 'cache', this.value);
        }
    }
    $('#search-input').on('inputchange', function() {
        $loadingGif = baseUrl + "/wp-content/themes/FH-Theme/img/loading.gif";
        if($("#loading").length == 0){
            $("form[name=search-form]").after("<div id='loading' style='text-align:center;'><img src='" + $loadingGif + "' alt='loading...' /></div>");
        }

        valSearch = $(this).val();
        type = $('#search-box input[name="type"]:checked').val();

        setTimeout(function(){

            searchable = true;

            if (valSearch.length >=3 && searchable) {
                  searchAjax(valSearch,type);
            }

        },1500);

        searchable = false;

    });

});


function showMessage(msg){
    $('#messages p').html(msg);
    $('#messages').animate({top: '100px'}, 300).delay(5000).animate({top: 0},300);
}

function setCookie(toBook,projectId,type,content,id){
    $.ajax({  
        url: baseUrl + "/wp-admin/admin-ajax.php",
        type:'POST',  
        data: "action=bookmark_it&projectId=" + projectId + "&type=" + type + "&content=" + content + "&id=" + id,
        success: function(response){
            var data = JSON.parse(response);
            if(data.error === 0){
                if(!id){
                    $.cookie("bookmark",parseInt(data.id), {expires: 30, path: '/'});
                }
                toBook.addClass('bookmarked');
                if($('header .top-nav .bookmark.bandeau').length){
                    $('header .top-nav .bookmark a > div').html(data.total);
                }else{
                    $('header .top-nav .bookmark').addClass('bandeau');
                    $('header .top-nav .bookmark a').prepend('<div>' + data.total + '</div>');
                }
                showMessage('This item has been bookmarked. You can find all your bookmarked items in the "Bookmarks" section.');
            }else{
                showMessage('This has already been bookmarked.');
                toBook.addClass('bookmarked');
            }
        }
    });
}


function searchAjax(val,type){
    $.ajax({  
        url: baseUrl + "/wp-admin/admin-ajax.php",
        type:'POST',  
        data: "action=search_it&str=" + val + "&type=" + type,   
        success: function(html){
            $('#loading').remove();
            $('#search-result').html(html);
            $('#search-result').fadeIn('fast');
            $('#search-box').css('position','absolute');
            $('#search-box, #search-result').css('opacity','1');
            $('.main').fadeOut('fast');
            $('footer').fadeOut('fast');

        }  
    });
}

function setUpHoverEffect(){
    $(".project").each(function(){
        $(this).unbind("mouseenter");

        $(this).on("mouseenter", function(){
            $blockOffset = $(this).offset();

            previousBlockPos.x = currentBlockPos.x;
            previousBlockPos.y = currentBlockPos.y;

            currentBlockPos.x = $blockOffset.left;
            currentBlockPos.y = $blockOffset.top;

            previousDirection = direction;

            if(previousBlockPos.y < currentBlockPos.y){
                direction = 0;
            }else if ( previousBlockPos.x > currentBlockPos.x){
                direction = 1;
            }else if ( previousBlockPos.y > currentBlockPos.y){
                direction = 2;
            }else{
                direction = 3;
            }

            var selected = $(".project-container .selected");
            changeBlockState("", selected, "out", ((direction + 2) % 4));
            selected.removeClass("selected");
            $(this).addClass('selected');
        });
    });
}

function scrollToDiv(id){
    var div = $(id);
    $('html,body').animate({scrollTop: div.offset().top-100},'500');
}

function resizeTitleBorders(){

    setTimeout(function(){
        $('.title-border-top').each(function(){
            var el = $(this);
            var marginLeft = 0;

            if(el.next('h1').html()){
                marginLeft = el.next('h1').offset().left;
            }else if(el.next('h2').html()){
                marginLeft = el.next('h2').offset().left;
            }
            el.css('margin-left',marginLeft);
            el.nextAll('.title-border-bottom').css('margin-left',marginLeft);
        });
    },80);

}

function resizeProjectBoxes(){
    var windowWidth = $(window).width();
    var projectBoxX = 0;
    var projectBoxY = 0

    if(windowWidth >= 960 && windowWidth <= 1024){

        projectBoxX = windowWidth/2;
        projectBoxY = Math.floor(projectBoxX*0.76);

        $('.project-container .project').css('width',Math.floor(projectBoxX));

    }else if(windowWidth > 1024 && windowWidth <= 1440){

        projectBoxX = windowWidth/3;
        projectBoxY = Math.floor(projectBoxX*0.76);

        if(screenX%3 == 2){
            $('.project-container .project').css('width',Math.floor(projectBoxX));
            $('.project-container .project:nth-child(3n+2)').css('width',Math.floor(projectBoxX) + 1);
            $('.project-container .project:nth-child(3n+3)').css('width',Math.floor(projectBoxX) + 1);
        }else if(screenX%3 == 1){
            $('.project-container .project').css('width',Math.floor(projectBoxX));
            $('.project-container .project:nth-child(3n+3)').css('width',Math.floor(projectBoxX) + 1);
        }else{
            $('.project-container .project').css('width',Math.floor(projectBoxX));
        }

    }else if(windowWidth > 1440){
        projectBoxX = windowWidth/4;
        projectBoxY = Math.floor(projectBoxX*0.76);

        $('.project-container .project').css('width',Math.floor(projectBoxX));
    }

//    $('.project-container .project').css('height',projectBoxY);
    if(!loaded){
        $('.project-container .project').animate({height: projectBoxY+'px'}, 2000);
        loaded = true;
    }
    
    $('.project-container .project').mouseover(function(){
        $(this).find('a h3').css('color', '#f01776');
    }).mouseleave(function(){
        $(this).find('a h3').css('color', '#373437');
    });

//    scaleProjects();
}

function scaleProjects(){
    $('.project-container .project a').delay(500).show('scale', 900);
}

function addRollover(){
    setUpHoverEffect();
    var nodes  = document.querySelectorAll('.project-container li'),
        _nodes = [].slice.call(nodes, 0);

    _nodes.forEach(function (el) {
        el.addEventListener('mouseover', function (ev) {
            changeBlockState( ev, this, 'in', direction );
        }, false);
    });
}

function removeAllClasses(obj){
    $(obj).removeClass('in-top');
    $(obj).removeClass('in-right');
    $(obj).removeClass('in-bottom');
    $(obj).removeClass('in-left');
    $(obj).removeClass('out-top');
    $(obj).removeClass('out-right');
    $(obj).removeClass('out-bottom');
    $(obj).removeClass('out-left');
}

function changeBlockState (ev, obj, state, d) {

    var class_suffix = "";

    removeAllClasses(obj);
    switch ( d ) {
        case 0 : class_suffix = '-top';    break;
        case 1 : class_suffix = '-right';  break;
        case 2 : class_suffix = '-bottom'; break;
        case 3 : class_suffix = '-left';   break;
    }

    var newClass = state + class_suffix;

    $(obj).addClass( newClass );
}

/* 
 * Create HTML5 elements for IE's sake
 */

document.createElement("article");
document.createElement("section");