$(document).ready(function(){

    $window = $(window);
    var windowHeight = $(window).height();
    
    var height = windowHeight-100;
    
    $('#home').css({height: height + 'px'});
    $('.cb-slideshow li span').css({
        height: height + 'px',
        backgroundSize : 'cover'
    });
    $('.nav .menu > li ul').css({top: '35px'});
    $('.nav .menu > li').hover(
        function(){
            $(this).find('ul').css({top: '-100px'});
        },
        function(){
            $(this).find('ul').css({top: '35px'});
        }
    );
    
    $('.cb-slideshow li:nth-child(1)').animate({opacity: 1},1000);
    
    var initSlideshow = setInterval(startSlideshow, 7000 );
    
    $(window).scroll(function() {
        if($(this).scrollTop() >= 1){
            $('.nav .menu > li ul').css({top: '35px'});
            $('.nav .menu > li').hover(
                function(){
                    $(this).find('ul').css({top: '100px'});
                },
                function(){
                    $(this).find('ul').css({top: '35px'});
                }
            );
        }else if($(this).scrollTop() < 1){
            $('.nav .menu > li ul').css({top: '35px'});
            $('.nav .menu > li').hover(
                function(){
                    $(this).find('ul').css({top: '-100px'});
                },
                function(){
                    $(this).find('ul').css({top: '35px'});
                }
            );
        }
        el = $('header'); 
        if ($(this).scrollTop() >= height && el.css('position') != 'fixed'){ 
            $('header').css({'position': 'fixed', 'top': '0px', 'left': '0px', 'width': '100%'});
            $('#content').css({'margin-top': '100px'});
        }else if($(this).scrollTop() < height && el.css('position') == 'fixed'){
            $('header').css({'position': 'relative'});
            $('#content').css({'margin-top': '0'});
        }
        if ($(this).scrollTop() >= height){
            $('#search-box').css({'position': 'fixed', 'top': '0'});
        }else{
            $('#search-box').css({'position': 'absolute', 'top': 'auto'});
        }
    });
                
    $('section[data-type="background"]').each(function(){
        var $bgobj = $(this);
                    
        $(window).scroll(function() {
                    								
            yPos = -($window.scrollTop() / $bgobj.data('speed')); 
            coords = '50% '+ yPos + 'px';

            $bgobj.find('ul li span').css({ backgroundPosition: coords });
                    
        });

    });
    
    $('#home .scroll-down').mouseover(function(){
        $('#home .scroll-down div').not(':last-child').addClass('blink');
    });
    $('#home .scroll-down').mouseout(function(){
            $('#home .scroll-down div').removeClass('blink');
    });
    setTimeout(function(){
        $('#home .home-bg').animate({opacity: '0'}, 1500, function(){
            $(this).remove();
        });
    },7000);
    
    $('#home .scroll-down').click(function(){
        $(this).animate({opacity: '0'}, 1500);
        $('#home .slogan').animate({opacity: '0'}, 1500);
        $('#home .title-border-top').animate({opacity: '0'}, 1500);
        $('#home .title-border-bottom').animate({opacity: '0'}, 1500);
        $('html, body').animate({
            scrollTop: $("header").offset().top
        }, 2000);
    });
    
    if($('.cb-slideshow iframe').length > 0){
        var f = $('.cb-slideshow iframe'),
            url = f.attr('src').split('?')[0],
            fIndex = 0;
    }

    $('.cb-slideshow li').each(function(){

        $(this).click(function(){

            if($(this).find('span > div.video').length > 0){
                
                if($('#home .slogan').length){
                    $('#home .slogan').animate({opacity: '0'}, 1500);
                    $('#home .title-border-top').animate({opacity: '0'}, 1500);
                    $('#home .title-border-bottom').animate({opacity: '0'}, 1500);
                }
                
                f = $(this).find('iframe');
                url = f.attr('src').split('?')[0];
                fIndex++;
                
                clearInterval(initSlideshow);
                f.show();
                
                post('addEventListener', 'pause');
                post('addEventListener', 'finish');
                post('addEventListener', 'playProgress');
                post('play');
            }
            
        });
    
    });

    // Listen for messages from the player
    if (window.addEventListener){
        window.addEventListener('message', onMessageReceived, false);
    }
    else {
        window.attachEvent('onmessage', onMessageReceived, false);
    }

    // Handle messages received from the player
    function onMessageReceived(e) {
        var data = JSON.parse(e.data);

        switch (data.event) {
            case 'ready':
                onReady();
                break;

            case 'playProgress':
                onPlayProgress(data.data);
                break;

            case 'pause':
                onPause();
                break;

            case 'finish':
                onFinish();
                break;
        }
    }
    
    function post(action, value) {
        var data = { method: action };

        if (value) {
            data.value = value;
        }

        f[0].contentWindow.postMessage(JSON.stringify(data), url);
    }
    
    function onReady() {
        post('addEventListener', 'pause');
        post('addEventListener', 'finish');
        post('addEventListener', 'playProgress');
    }

    function onPause() {
        $('.cb-slideshow li iframe').hide();
        initSlideshow = setInterval(startSlideshow, 7000 );
    }

    function onFinish() {
        $('.cb-slideshow li iframe').hide();
        initSlideshow = setInterval(startSlideshow, 7000 );
    }

    function onPlayProgress(data) {
        //status.text(data.seconds + 's played');
    }
    
    function startSlideshow(){
        var $active = $('.cb-slideshow li.active');

        if ( $active.length == 0 ) $active = $('.cb-slideshow li:last-child');

        var $next = $active.next().length ? $active.next() : $('.cb-slideshow li:first-child');

        $active.addClass('last-active');

        $next.addClass('active')
            .animate({opacity: 1}, 1500, function() {
                $active.removeClass('active last-active');
                $active.animate({opacity: 0}, 1500);
            });
    }
    
});