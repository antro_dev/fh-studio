<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
		
		<!-- dns prefetch -->
		<link href="//www.google-analytics.com" rel="dns-prefetch">
		
		<!-- meta -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		
		<!-- icons -->
                <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon@x2.png" />
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/icon_128x128.png" rel="apple-touch-icon-precomposed">
			
		<!-- css + javascript -->
		<?php wp_head(); ?>
                
		<script>
                    !function(){
                            // configure legacy, retina, touch requirements @ conditionizr.com
                            conditionizr();
                    }()
                    
    
                    $(window).scroll(function() {

                            el = $('header');
                            if ($(this).scrollTop() >= 1 && el.css('position') != 'fixed'){ 
                                el.css({'position': 'fixed', 'top': '0px', 'left': '0px', 'width': '100%'});
                                $('section').css({'margin-top': '100px'});
                            }else if($(this).scrollTop() == 0 && el.css('position') == 'fixed'){
                                el.css({'position': 'relative'});
                                $('section').css({'margin-top': '0'});
                            }
                    });
		</script>
	</head>
	<body <?php body_class(); ?>>
	
		<!-- wrapper -->
		<div class="wrapper">
	
			<!-- header -->
			<header class="header clear" role="banner">
				
                            
                                <?php
                                    if($_COOKIE['bookmark']){
                                        global $wpdb;
                                        $bookCount = $wpdb->get_var('SELECT COUNT(*) FROM wp_bookmarks WHERE cookie_id = ' . $_COOKIE['bookmark'] . ' OR id = ' . $_COOKIE['bookmark']);
                                    }
                                ?>
					<div class="top-nav">
                                            <ul>
                                                <li><a href="#" class="newsletter">Newsletter</a></li>
                                                <li class="bookmark<?php echo $bookCount? ' bandeau' : ''; ?>">
                                                    <a href="<?php echo get_permalink(1875) ?>">
                                                        <?php if($bookCount): ?>
                                                        <div><?php echo $bookCount ?></div>
                                                        <?php endif; ?>
                                                        Bookmarks
                                                    </a>
                                                </li>
                                                <li class="search"><a href="#">Search</a></li>
                                                <?php foreach(icl_get_languages('skip_missing=0') as $lng): ?>
                                                    <?php if($lng['active'] != 1): ?>
                                                    <li><a href="<?php echo $lng['url'] ?>"><?php echo $lng['native_name'] ?></a></li>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <li class="social facebook"><a href="http://facebook.com/fhstudio" target="_blank"></a></li>
                                                <li class="social twitter"><a href="http://twitter.com/fhstudi0" target="_blank"></a></li>
                                                <li class="social pinterest"><a href="http://www.pinterest.com/fhstudi0" target="_blank"></a></li>
                                            </ul>
                                        </div>

                                        <a href="<?php echo get_bloginfo('url') ?>" class="logo-link"><div class="logo"></div></a>

                                        <nav class="nav" role="navigation">
                                                <?php wp_nav_menu( array( 'menu' => 'main' ) ); ?>
                                        </nav>
			
			</header>
			<!-- /header -->
                        
                        <div id="messages">
                            <p>This is a message to advise that there is an issue...</p>
                        </div>
                        
                        <div id="search-box">
                            
                            <form name="search-form" method="post" action="#">
                            
                                <div class="close-search"></div>

                                <div class="title-border-bottom"></div>

                                <div class="search-for">Search On FH-studio</div>
                                <div class="start-typing">Start typing</div>

                                <input type="text" name="search" id="search-input" value="" placeholder="Search..." autocomplete="off" />

                                <div class="type">
                                    <div>
                                        <input type="radio" name="type" value="projects" id="projects-type" checked="checked" /><label for="projects-type" class="selected underline">Projects</label>
                                    </div>
                                    <div>
                                        <input type="radio" name="type" value="talents" id="artists-type" /><label for="artists-type" class="underline">Artists</label>
                                    </div>
                                    <div>
                                        <input type="radio" name="type" value="post" id="posts-type" /><label for="posts-type" class="underline">Articles</label>
                                    </div>
                                </div>
                            
                            </form>
                            
                            <div id="search-result"></div>
                        </div>