<?php 

    /* Template Name: Production */ 

    get_header(); 
    
?>

<section id="content" class="production main">
    
    <?php
        $args = array(
            'post_type' => 'projects',
            'meta_query' => array(
                array(
                    'key' => 'wpcf-project-display-in-production',
                    'value' => 1,
                    'compare' => 'is',
                ),
            ),
            'orderby' => 'post_date',
            'order' => 'DESC',
        );
    ?>
    <?php $projects = new WP_query($args); ?>
    
    <div class="filter">
        
        <div class="filter-type">
            <span class="title">Genre</span>

            <ul>
                <li><a href="#filter" data-filter="*" class="selected underline">All</a></li>

                <li><a href="#filter" data-filter=".tag-video" class="underline" >Videos</a></li>

                <li><a href="#filter" data-filter=".tag-photo" class="underline" >Photos</a></li>
            </ul>
        </div>
        
        <div class="filter-category">
            <span class="title">Tags</span>
            
            <?php 
            global $wpdb;
            $metatable = $wpdb->prefix."options";
            $tags = $wpdb->get_results("SELECT option_value FROM $metatable where option_name='wpcf-fields'");

            foreach($tags as $tag){
                $projectTypes = unserialize($tag->option_value);
                $tags = $projectTypes['project-tag']['data']['options'];
            }
            
            ?>
            
            <ul>
            
                <li><a href="#filter" data-filter="*" class="selected underline">All</a></li>

                <?php $i=1; foreach($tags as $tag): ?>

                <li><a href="#filter" data-filter=".<?php echo strtolower('tag-' . $tag['set_value']) ?>" class="underline" ><?php echo $tag['title'] ?></a></li>

                <?php $i++; endforeach; ?>
            
            </ul>
            
            <div class="more"><a href="#">More</a></div>
            
        </div>
        
    </div>
            
    <?php if ($projects) : ?>
    
    <div class="project-container">
        
        <ul>
        
        <?php while ($projects->have_posts()) : $projects->the_post(); ?>
        
        <?php $parent_talent_id = wpcf_pr_post_get_belongs($post->ID, 'talents') ?>
        <?php $tags = get_post_meta($post->ID,'wpcf-project-tag') ?>
            
            <li class="project <?php echo (get_post_meta($post->ID,'wpcf-type',TRUE) && get_post_meta($post->ID,'wpcf-type',TRUE) != 'both')? 'tag-' . get_post_meta($post->ID,'wpcf-type',TRUE) : 'tag-video tag-photo' ?><?php foreach($tags[0] as $tag) echo strtolower(' tag-' . $tag); ?>">
            <a class="normal" href="<?php echo get_permalink() ?>" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>');">

                <div<?php echo (get_post_meta($post->ID,'wpcf-type',TRUE) == 'video' || get_post_meta($post->ID,'wpcf-type',TRUE) == 'both')? ' class="video"' : '' ?>>
                    <div>
                        <h3><?php the_title(); ?></h3>
                        <span><?php echo get_the_title(($parent_talent_id)? $parent_talent_id : 'Production') ?></span>
                    </div>
                </div>

            </a>
            <div class="background"></div>
        </li>
        
        <?php endwhile; ?>
            
        </ul>
        
    </div>
    
    <?php endif; ?>

</section>

<?php get_footer('empty'); ?>

<script type="text/javascript">
    var windowW = $(window).width();
    var count = 2;
    var divProjectsTop = $('.project-container').position().top+100;
    
    $(document).ready(function(){
        resizeProjectBoxes();
        addRollover();
        
        $('.filter-category').css('width',windowW-290);
        $('.filter-category ul').css('width',windowW-290-250);
        
        var closed = true;
        $('.filter .more').click(function(e){
            e.preventDefault();
            if(closed === true){
                $('.filter').animate({height: '100px'},300);
                closed = false;
            }else{
                $('.filter').animate({height: '65px'},300);
                closed = true;
            }
            
        });
        
    });
    
    $(window).scroll(function(){  
        if  ($(window).scrollTop() === $(document).height() - $(window).height()){  
           loadArticle(count);
           count++;  
        }  
    });

    $(window).resize(function(){
        resizeProjectBoxes();
    });
    
    function loadArticle(pageNumber) {  
        $.ajax({  
            url: "<?php bloginfo('wpurl') ?>/wp-admin/admin-ajax.php",
            type:'POST',  
            data: "action=infinite_scroll&page_no="+ pageNumber + '&loop_file=project&type=prod',   
            success: function(html){
                var $newItems = html;
                $('.production .project-container ul').append( $newItems ); //.isotope( 'addItems', $newItems ).isotope( 'reloadItems' );
                loaded = false;
                resizeProjectBoxes();
                addRollover();
            }  
        });  
        return false;  
    } 
</script>

