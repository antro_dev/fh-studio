<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fhstudio');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'z*ujKXFw1P?yM@1ohp+8X12-GQ?C>yh-0Iec7h:{T0K3--LFsK7cqUe32--DAbDE');
define('SECURE_AUTH_KEY',  'Ht[DCPsVlhwB&tdAdU-4FgJaU+@y18apx-o9mL9/)V<.OI[#2n+KxQtMTR7]cGi-');
define('LOGGED_IN_KEY',    '.91YwP;.KK{YDLSA>?U:W<GhMy7-{19@U+qZ+lj+=(]8(k_~|4QQ.3+&f-Hg&=er');
define('NONCE_KEY',        'HdCD^wAC/$T[V.F576d+k@e*?Y]OMl=71UcR?bn@QSC^79VsnMZ5-n|1%0QaER<3');
define('AUTH_SALT',        ')95<HZ/={f//>dc:YlG,4}DF]bZARE@9bA,?+y,.@,qrWhOW#1e>BmfO}HU+Y34m');
define('SECURE_AUTH_SALT', 'N9U-j#4Xrb,YjP>j-mr8My?am|Xa`:_:N8rqbj8pF2Ge3na#cCahu-|y!g>jk5Wi');
define('LOGGED_IN_SALT',   'a#+Z]w83#|:xiEStl#RhtK#3MdAcQeDY8gX4-}z`@/(ANn@TF(ZWP`W5L+J=j[(J');
define('NONCE_SALT',       '2i t]H-fNU4vb8uC=q#0z:R|*fp|kyD0`6~B<Sm/qvaLCip+8f_>&L?yj8U5wgxM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
